#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Date : 2020-01-15
# @Author :klingeling

import base64
import hashlib
import hmac
import time
import urllib.parse
from dingtalkchatbot.chatbot import DingtalkChatbot
from stock import StockInfo


class DingDing:
    """钉钉机器人"""
    def __init__(self, message):
        timestamp = round(time.time() * 1000)
        secret = 'SEC5852e6abd0f07be8f4194bd11101c30a3063d087e256ca7249f9826187340c01'
        access_token = 'e895958745648cd423a75574d21da4dad42619b2d226512567e9e93f8591a096'
        secret_enc = secret.encode('utf-8')
        # 把timestamp+"\n"+密钥当做签名字符串
        string_to_sign = '{}\n{}'.format(timestamp, secret)
        string_to_sign_enc = string_to_sign.encode('utf-8')
        # 使用HmacSHA256算法计算签名
        hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
        # 进行Base64 encode把签名参数再进行urlEncode
        sign = urllib.parse.quote_plus(base64.b64encode(hmac_code))
        url = f'https://oapi.dingtalk.com/robot/send?access_token={access_token}&timestamp={timestamp}&sign={sign}'
        self.__Eyjafjalla = DingtalkChatbot(url)
        self.message = message

    def send(self):
        self.__Eyjafjalla.send_text(msg=self.message, is_at_all=False)
