#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Date : 2020-01-15
# @Author :klingeling

from stock import StockInfo
from dingding import DingDing
from warning import MyWarning
# def main():
#     war = MyWarning()
#     sto = StockInfo(code=war.list['code'], profit_ceiling_price=float(war.list['profit_ceiling_price']), stop_price=float(war.list['stop_price']))
#     if sto.is_open():
#         ding = DingDing(sto.message())
#         ding.send()


def main():
    sto = StockInfo(code='000581', profit_ceiling_price=float(34), stop_price=float(32))
    if sto.is_open():
        ding = DingDing(sto.message())
        ding.send()


main()
