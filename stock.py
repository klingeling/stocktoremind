#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Date : 2020-01-15
# @Author :klingeling

import tushare as ts
import lxml
import pandas
import bs4
import re
import time


class StockInfo:
    """股票信息"""
    def __init__(self, code, profit_ceiling_price, stop_price):
        self.code = code
        self.profit_ceiling_price = profit_ceiling_price
        self.stop_price = stop_price
        # a = re.search(r'SZ|SH', code).group()
        # b = re.search(r'\d{6}', code).group()
        # self.num = b
        # if a == 'SZ':
        #     self.area = 'SZSE'
        # elif a == 'SH':
        #     self.area = 'SSE'
        # else:
        #     pass

    time = time.strftime('%Y%m%d', time.localtime(time.time()))
    __pro = ts.pro_api('163f1611eb545b0b9317e566f54fb427135588a2d749ecdb7ef8959b')
    # 初始化pro接口

    def is_open(self):
        # a = self.__pro.trade_cal(exchange=self.area, start_date=self.time, end_date=self.time)
        a = self.__pro.trade_cal(start_date=self.time, end_date=self.time)
        return a.is_open[0]

    def get_price(self):
        df = ts.get_realtime_quotes(self.code)
        return df.price[0]

    def get_name(self):
        df = ts.get_realtime_quotes(self.code)
        return df.name[0]

    def message(self):
        realtime_price = float(self.get_price())
        name = self.get_name()
        """
        若高于则给钉钉发出卖出信号。如低于则发出买入信号！
        :param profit_ceiling_price: 止盈价格
        :param stop_price:           止损价格
        :param realtime_price:       实时价格
        """
        if realtime_price > self.profit_ceiling_price:
            return f'股票:{name}, 代码:{self.code}, 现价:{realtime_price} 大于期望:{self.profit_ceiling_price}, 看好就收，快抛！！'
        elif realtime_price < self.stop_price:
            return f'股票:{name}, 代码:{self.code}, 现价:{realtime_price} 小于期望:{self.stop_price}，买买买！'
        else:
            return None

