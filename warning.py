#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Date : 2020-01-15
# @Author :klingeling

import configparser


class MyWarning:
    def __init__(self):
        conf = configparser.ConfigParser()
        # conf["DEFAULT"] = {'code': '000581',
        #                    'profit_ceiling_price': 34,
        #                    'stop_price': 32
        #                    }  # 类似于操作字典的形式
        conf.read('config.ini', encoding='utf-8')
        self.dir = {}
        sections = conf.sections()
        for section in sections:
            for option in conf.options(sections):
                self.list.update({option: conf.get(section, option)})
